import {Command, flags} from '@oclif/command'
import * as os from 'os'
import * as path from 'path'
import * as scraper from 'website-scraper'
import * as URI from "uri-js"
import { cli } from 'cli-ux'
import * as fs from 'fs-extra'

class ScrapperPlugin {
  apply(registerAction: (event: string, callback: Function) => any ){
    registerAction('error', async ({error}: {error: string}) => { console.error(error) });
    registerAction('onResourceSaved', ({resource}: {resource: { url: string} }) => console.log(`Downloaded: ${resource.url}`));
    registerAction('onResourceError', ({resource, error}: {resource: { url: string}, error: string }) => console.error(`Error Downloading: ${resource.url}\nError Message: ${error}`));
  }
}
class WebsiteGrabber extends Command {
  static description = 'describe the command here'

  static flags = {
    help: flags.help({char: 'h'}),
  }

  static args = [
    {
      name: 'url',
      required: true,
      description: 'Target website URL'
    }
  ]

  async run() {
    const {args: {url}} = this.parse(WebsiteGrabber)
    const {host, path : urlPath} = URI.parse(url)
    if (!host) { return this.error("Uh Oh! The URL is not valid. Please try again ") }
    const hostName = host.match(/\.(.*?)\./i)![1]
    const templateName = urlPath ? urlPath.split('/').slice(-1).pop()!.replace(".html", "") : ""
    const targetPath = path.join(`${os.homedir()}`, `Desktop`, `${hostName}`, `${templateName}`)
    await fs.remove(targetPath)
    const grabberOptions = {
      urls: [url],
      directory: path.join(`${os.homedir()}`, `Desktop`, `${hostName}`, `${templateName}`),
      plugins: [new ScrapperPlugin()],
      recursive: true,
      maxRecursiveDepth: 1
    }

    cli.action.start('Downloading Your Website', "", {stdout: true})
    await scraper(grabberOptions)
    cli.action.stop()
    this.log(grabberOptions.directory)
  }
}

export = WebsiteGrabber
